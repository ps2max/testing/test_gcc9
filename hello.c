#include <stdio.h>
#include <kernel.h>

#define HACK

#ifdef HACK
s32 gsema;
#endif

static void _sleep_waker(s32 alarm_id, u16 time, void *arg2)
{
#ifdef HACK
    s32 *pSema = &gsema;//(s32 *)arg2;
#else
    s32 *pSema = (s32 *)arg2;
#endif
    iSignalSema(*pSema);
}

int main()
{
    ee_sema_t sema;
    s32 sema_id;

	printf("Testing kernel callback function\n");

	sema.init_count = 0;
	sema.max_count  = 1;
	sema.option     = 0;
	sema_id = CreateSema(&sema);

#ifdef HACK
	gsema = sema_id;
#endif

	SetAlarm(1, _sleep_waker, &sema_id);
	WaitSema(sema_id);
	DeleteSema(sema_id);

	printf("Done!\n");
	while(1){}

	return 0;
}
