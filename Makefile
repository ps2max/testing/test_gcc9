EE_BIN = hello.elf
EE_OBJS = hello.o
EE_CFLAGS = -D_EE -O2 -G0 -Wall -I$(PS2SDK)/ee/include -I$(PS2SDK)/common/include 
EE_ASFLAGS = -G0 -msingle-float
EE_LDFLAGS = -L$(PS2SDK)/ee/lib

all: $(EE_BIN)

clean:
	rm -f $(EE_BIN) $(EE_OBJS)

sim: $(EE_BIN)
	PCSX2 --elf=${PWD}/$(EE_BIN)

run: $(EE_BIN)
	ps2client -h 192.168.1.10 execee host:$(EE_BIN)

%.o: %.c
	$(EE_CC) $(EE_CFLAGS) $(EE_INCS) -c $< -o $@

%.o: %.S
	$(EE_CC) $(EE_CFLAGS) $(EE_INCS) -c $< -o $@

%.o: %.s
	$(EE_AS) $(EE_ASFLAGS) $< -o $@

$(EE_BIN): $(EE_OBJS)
	$(EE_CC) $(EE_CFLAGS) -o $(EE_BIN) $(EE_OBJS) $(EE_LDFLAGS) $(EE_LIBS)

include $(PS2SDK)/samples/Makefile.pref
